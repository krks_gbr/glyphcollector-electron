

import React from 'react';
import {connect} from 'react-redux';
import style from './style.scss';
import Button from '../../components/button';
import {push} from 'react-router-redux';
import * as actions from '../../actions/app';



const Collect = props => {


    const {dispatch, app} = props;
    const {currentPair, collectProgress} = app;


    console.log('rendering collect');


    const goBack = () => {
        dispatch(actions.cancelCollect());
        dispatch(push('/'));
    };


    const percent = Math.ceil(collectProgress*100);
    return (
        <div className={style.collect}>
            <div className={style.progressContainer}>
                <div className={style.progress} style={{width: `${collectProgress*100}%`}}></div>
                <div className={style.progressText}>{percent}%</div>
            </div>
            <div className={style.header}>Comparing:</div>
            {
                currentPair &&
                <div className={style.thumbs}>
                    <div className={style.scanThumb}>
                        <img src={`file://${currentPair.scan}`}/>
                    </div>
                    <div className={style.sampleThumb}>
                        <img src={`file://${currentPair.sample}`}/>
                    </div>
                </div>
            }
            <div className={style.footer}>
                <Button onClick={goBack} noborder label="back"/>
            </div>
        </div>
    )

};



export default connect(state => ({app: state.app}))(Collect);




