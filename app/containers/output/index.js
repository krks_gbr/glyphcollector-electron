





import React from 'react';
import style from './style.scss';
import {connect} from 'react-redux';
import Button from '../../components/button';
import fs from 'fs';
import * as actions from '../../actions/app';
import {push} from 'react-router-redux';




const Output = props => {

    const {dispatch, app} = props;
    const {output} = app;
    const {selectedLetter, paths, letters} = output;


    const glyphPath = `${paths.glyphs}/${selectedLetter}`;
    const glyphImgs = fs.readdirSync(glyphPath).map(img => `file://${glyphPath}/${img}`);

    const avgPath = `${paths.averages}/${selectedLetter}.jpg`;

    const vectorPath = `${paths.vectors}/${selectedLetter}.svg`;


    const headerItems = letters.split('').map(l => {
        return <Button onClick={() => dispatch(actions.selectLetter(l))} key={l} label={l}/>
    });

    const goBack = () => {
        dispatch(actions.cancelCollect());
        dispatch(push('/'));
    };


    if(output.fontExportStatus !== 'IDLE'){

        if(output.fontExportStatus === 'CALCULATING_SPACING'){
            return <div className={style.modal}>Calculating spacing...</div>
        }

        if(output.fontExportStatus === 'EXPORTING_FONT'){
            return <div className={style.modal}>Exporting font...</div>
        }

    }

    if(output.status !== 'DONE'){
        if(output.status === 'CALCULATING_AVG'){
            return <div className={style.modal}>Calculating averages...</div>
        }

        if(output.status === 'CALCULATING_OUTLINES'){
            return <div className={style.modal}>Calculating outlines...</div>
        }

        // return <div>error</div>
    }

    return (
        <div className={style.output}>
            <div className={style.header}>
                {headerItems}
            </div>
            <div className={style.body}>
                 <div className={style.glyphsContainer}>
                        <Glyphs images={glyphImgs}/>
                    </div>

                    <div className={style.results}>
                        <div>
                            <div>Average:</div>
                            <div className={style.result}><img src={avgPath}/></div>
                        </div>
                        <div>
                            <div>Vector:</div>
                            <div className={style.result}><img src={vectorPath}/></div>
                        </div>
                    </div>
            </div>
            <div className={style.footer}>
                <Button onClick={goBack} noborder label="back"/>
                <Button onClick={() => dispatch(actions.exportFont())} noborder label="export font"/>
            </div>
        </div>
    );


};




const Glyphs = ({images}) => {


    let imageContainers = images.map((img,i) => {
        return (
            <div key={i} className={style.glyphThumbContainer}>
                <img src={img}/>
            </div>
        )
    });

    return (
        <div className={style.glyphs}>
            {imageContainers}
        </div>
    );

};


export default connect(state => state)(Output);


