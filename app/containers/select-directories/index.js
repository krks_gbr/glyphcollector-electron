


import React, {Component} from 'react';
import Button from '../../components/button';
import {remote} from 'electron';
import {connect} from 'react-redux';
import * as actions from '../../actions/app';
import style from './style.scss';
import {push} from 'react-router-redux';



function renderThumbnails(thumbnails){
    return (
        <div className={style.thumbnails}>
           {
               thumbnails.map((th, i) =>
                   <div key={i} className={style.thumbnail} >
                       <img src={`file://${th}`}/>
                   </div>
               )
           }
        </div>
    )
}

const Home = props => {

    const {dispatch, app}  = props;
    console.log(app);

    const openDialog = callback => {
        const config = {properties: ['openDirectory'], buttonLabel: 'Select'};
        remote.dialog.showOpenDialog(config, path => { if(path) callback(path[0]) });
    };


    const selectScansPath = () => {
        openDialog( path => dispatch(actions.setScansDir(path)));
    };

    const selectSamplesPath = () => {
        openDialog( path => dispatch(actions.setSamplesDir(path)));
    };

    const proceed = () => {
        dispatch(actions.startCollect());
        dispatch(push('/collect'));
    };

    return (
        <div className={style.container}>
            <div className={style.thumbnailContainers}>
                <div style={{justifyContent: 'center'}} className={style.thumbnailsContainer}>
                    {
                        app.scans ? renderThumbnails(app.scans.files) :
                            <Button noborder fill onClick={selectScansPath} label="Load Scans"/>
                    }
                </div>
                <div className={style.thumbnailsContainer}>
                    {
                            app.samples ? renderThumbnails(app.samples.files) :
                            <Button noborder fill onClick={selectSamplesPath} label="Load Samples"/>
                    }
                </div>
                <div className={style.footer}>
                    <Button noborder onClick={proceed} disabled={!(app.scans && app.samples)} label="Find Instances"/>
                </div>
            </div>
        </div>
    );


};




export default connect(state => state)(Home);



