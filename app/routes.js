import React from 'react';
import {Route, IndexRoute} from 'react-router';
import Page from './containers/page';
import Collect from './containers/collect';
import Output from './containers/output';
import HomePage from './containers/select-directories';


export default (
    <Route path="/" component={Page}>
        <IndexRoute component={HomePage}/>
        <Route path="/collect" component={Collect}/>
        <Route path="/output" component={Output}/>
    </Route>
);
