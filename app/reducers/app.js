



import {Types} from '../actions/app';


const outputpath = '/Users/GaborK/projects/glyphcollector-electron/files/output';

const OUTPUT_STATUSES = {
    CALCULATING_AVG: 'CALCULATING_AVG',
    CALCULATING_OUTLINES: 'CALCULATING_OUTLINES',
    DONE: 'DONE'
};


const initialState = {
    samples: null,
    scans: null,
    isCollecting: false,
    hasCollected: false,
    currentSample: null,
    currentPair: null,
    collectProgress: 0,
    isExportingFont: false,
    output: {
        status: null,
        letters: 'acdeghmoprstu',
        paths: {
            averages: `${outputpath}/averages`,
            glyphs: `${outputpath}/glyphs`,
            vectors: `${outputpath}/vectors`
        },
        selectedLetter: 'a',
    }
};

export default function counter(state = initialState, action) {
    switch (action.type) {

        case Types.SET_SAMPLES_DIR:

            return {
                ...state,
                samples: action.payload,
            };


        case Types.SET_SCANS_DIR:

            return {
                ...state,
                scans: action.payload,
            };


        case Types.SET_CURRENT_PAIR:
            return {
                ...state,
                currentPair: action.payload
            };

        case Types.SET_COLLECT_PROGRESS:
            return {
                ...state,
                collectProgress: action.payload,
            };

        case Types.COLLECTION_START:
            return {
                ...state,
                collectProgress: 0,
                isCollecting: true,
                hasCollected: false,
            };

        case Types.COLLECTION_END:
            return {
                ...state,
                isCollecting: false,
                hasCollected: true,
                currentPair: null,
            };

        case Types.SET_OUTPUT_STATUS:{
            return {
                ...state,
                output:{
                    ...state.output,
                    status: action.payload
                }
            }
        }


        case Types.SELECT_LETTER:
            return {
                ...state,
                output:{
                    ...state.output,
                    selectedLetter: action.payload,
                }
            };

        case Types.SET_FONT_EXPORT_STATUS:
            return {
                ...state,
                output: {
                    ...state.output,
                    fontExportStatus: action.payload,
                }
            };

        case Types.EXPORT_FONT_END:
            return {
                ...state,
                output: {
                    ...state.output,
                    fontExportStatus: action.payload,
                }
            };

        default:
            return state;

    }
}
