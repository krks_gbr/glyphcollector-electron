

import fs from 'fs';
import path from 'path';
import _ from 'lodash';
import {push} from 'react-router-redux';



const Types = {
    SET_SAMPLES_DIR: 'SET_SAMPLES_DIR',
    SET_SCANS_DIR: 'SET_SCANS_DIR',
    COLLECTION_START: 'COLLECTION_START',
    COLLECTION_END: 'COLLECTION_END',
    SET_CURRENT_PAIR: 'SET_CURRENT_PAIR',
    SET_COLLECT_PROGRESS: 'SET_COLLECT_PROGRESS',
    SELECT_LETTER: 'SELECT_LETTER',
    SET_OUTPUT_STATUS: 'SET_OUTPUT_STATUS',
    SET_FONT_EXPORT_STATUS: 'SET_FONT_EXPORT_STATUS',
};

export {Types};


const acceptedFileExtensions = ['.jpg', '.jpeg'];



function readFiles(dirPath) {

    return new Promise((resolve, reject) => {

        fs.readdir(dirPath, (err, files) => {

            if(err){
                reject(err);
            }

            const fpaths =
                files
                    .filter(f => acceptedFileExtensions.includes(path.extname(f)))
                    .map(f => `${dirPath}/${f}`);

            resolve(fpaths);

        });

    });

}

export function setSamplesDir(dirPath) {
    return (dispatch, getState) => {


        readFiles(dirPath).then(files => {

            dispatch({
                type: Types.SET_SAMPLES_DIR,
                payload: {
                    dirPath: dirPath,
                    files: files,
                }
            });

        }).catch(err => console.log(err));

    }
}

export function setScansDir(dirPath) {
    return (dispatch, getState) => {


        readFiles(dirPath).then(files => {

            dispatch({
                type: Types.SET_SCANS_DIR,
                payload: {
                    dirPath: dirPath,
                    files: files,
                }
            });

        }).catch(err => console.log(err));

    }
}

export function cancelCollect(){
    if(work){
        clearInterval(work);
    }
    return {
        type: Types.COLLECTION_END
    }
}

let work = null;

export function startCollect(){
    let interval = 100;
    return (dispatch, getState) => {
        dispatch({ type: Types.COLLECTION_START });

        const app  = getState().app;
        const scans = app.scans.files;
        const samples = app.samples.files;
        let pairIndex = 0;

        const pairs =   _.chain(scans)
                        .map(scan => samples.map(sample => ({sample: sample, scan: scan})))
                        .flattenDeep()
                        .value();

        work = setInterval(() => {

            dispatch({
                type: Types.SET_CURRENT_PAIR,
                payload: pairs[pairIndex]
            });


            dispatch({
                type: Types.SET_COLLECT_PROGRESS,
                payload: pairIndex/pairs.length
            });

            if(pairIndex === pairs.length - 1){

                clearInterval(work);
                dispatch({
                    type: Types.COLLECTION_END,
                });

                dispatch(calculateResults());
                dispatch(push('/output'))

            } else {

                pairIndex++;

            }

        }, interval);

    }
}

export function calculateResults(){
    return (dispatch, getState) => {

        dispatch({
            type: Types.SET_OUTPUT_STATUS,
            payload: 'CALCULATING_AVG',
        });


        setTimeout(() => {


            dispatch({
                type: Types.SET_OUTPUT_STATUS,
                payload: 'CALCULATING_OUTLINES',
            });

        }, 2000);


        setTimeout(() => {


            dispatch({
                type: Types.SET_OUTPUT_STATUS,
                payload: 'DONE',
            });

        }, 5000);
    }
}


export function selectLetter(l){
    return {
        type: Types.SELECT_LETTER,
        payload: l
    }
}

export function exportFont(){

    return (dispatch, getState) => {

        dispatch({
            type: Types.SET_FONT_EXPORT_STATUS,
            payload: 'CALCULATING_SPACING'
        });

        setTimeout(() => {
            dispatch({
                type: Types.SET_FONT_EXPORT_STATUS,
                payload: 'EXPORTING_FONT'
            });
        }, 1000);

        setTimeout(() => {
            dispatch({
                type: Types.SET_FONT_EXPORT_STATUS,
                payload: 'IDLE'
            });
        }, 1500);

    }

}